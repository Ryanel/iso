﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {
	[SerializeField, Range(0.0f, 1.0f)]
	private float lerpRate;
	public float xRotation;
	public float yRotation;

	public float targetX;
	public float targetY;

	public float distance = 10f;
	public float height = 10f;
	public Transform target;
	public Camera cam;
	public int camAngle = 0;
	public bool lerpPosition = false;
	public bool changeZoom = false;
	LevelManager lvl;
	// Use this for initialization
	void Start () {
		targetX = transform.rotation.eulerAngles.x;
		targetY = transform.rotation.eulerAngles.y;
		lvl = GameObject.FindGameObjectWithTag ("Level").GetComponent<LevelManager> ();
		cam = GetComponent <Camera>();
		lerpRate *= 125;
		if(changeZoom)
		{
			cam.orthographicSize = lvl.zoomLevel;
		}
	}

	public void RotateLeft()
	{
		targetY -= 90;
		camAngle--;
	}
	public void RotateRight()
	{
		targetY += 90;
		camAngle++;
	}
	// Update is called once per frame
	void Update () {
        // Determine if we need to move
		if(GameInput.playerCameraLeft)
		{
			RotateLeft ();
		}
		if(GameInput.playerCameraRight)
		{
			RotateRight ();
		}
		if(GameInput.playerCameraDown)
		{
			targetX -= 15f;
			if (targetX < 15f) {
				targetX = 15f;
			}
		}
		if(GameInput.playerCameraUp)
		{
			targetX += 15f;
			if (targetX > 60f) {
				targetX = 60f;
			}
		}
		if (camAngle < 0) {
			camAngle = 3;
		}
		if (camAngle > 3) {
			camAngle = 0;
		}

        // Okay, let's apply it now.
		bool skipRotation = false;

        // Skip rotation of the camera (which is CPU expensive) if we don't need to.
		if(targetX == xRotation)
		{
			if (targetY == yRotation)
			{
				skipRotation = true;
			}
		}

		Quaternion rotation = transform.rotation;

        // Do rotation.
		if (skipRotation == false)
		{
            //Rotate towards target angle.
			xRotation = Mathf.Lerp(xRotation, targetX, lerpRate * Time.deltaTime);
			yRotation = Mathf.Lerp(yRotation, targetY, lerpRate * Time.deltaTime);

            // Snap if we're close.
			if (Mathf.Approximately(xRotation, targetX))
			{
				xRotation = targetX;
			}
			if (Mathf.Approximately(yRotation, targetY))
			{
				yRotation = targetY;
			}

            //Apply.
			rotation = Quaternion.Euler(xRotation, yRotation, 0);
			transform.rotation = rotation;
		}
		
        // Set position.
		Vector3 pos_modifier = new Vector3 (0, 0, -distance);
		Vector3 position = rotation * pos_modifier + target.position;
		position.y += height;
        
        // If we're smoothing position, do it here. Only the main menu lerps.
		if(lerpPosition)
		{
			transform.position = Vector3.Lerp(transform.position, position, lerpRate * Time.deltaTime);
		}
		else
		{
			transform.position = position;
		}
		
	}

	void FixedUpdate()
	{
        // Handles zoom effect.
		if (!lvl.hasReachedGoal) {
			distance += 0.4f;
			if (distance > 15) {
				distance = 15;
			}
		} else {
			distance -= 0.4f;
			if (distance < -50) {
				distance = -50;
			}
		}
	}
}
