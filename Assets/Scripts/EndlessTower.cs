﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class EndlessTower : MonoBehaviour {
	public Transform cameraPosition;
	public Vector3 currentGenWorldPosition;
	public Vector3 incrementAmount;
	public List<GameObject> towerPrefabs;
	public int numLoadedSections = 0;
	public int numLoadedSectionsTarget = 5;
	List<GameObject> loadedSections;

	// Use this for initialization
	void Start () {
		currentGenWorldPosition = Vector3.zero;
		loadedSections = new List<GameObject> ();
		while (numLoadedSections < numLoadedSectionsTarget) {
			CreateSection (currentGenWorldPosition);
			currentGenWorldPosition += incrementAmount;
		}

	}

	void CreateSection(Vector3 worldPosition)
	{
		numLoadedSections++;
		GameObject g = (GameObject)Instantiate (ChoseTowerSection(), worldPosition,Quaternion.Euler(0f,0f,0f), this.gameObject.transform);
		loadedSections.Add (g);
	}

	GameObject ChoseTowerSection()
	{
		return towerPrefabs[Random.Range (0, towerPrefabs.Count)];

	}

	void DestroyLowestSection()
	{
		GameObject lowest = null;
		foreach (GameObject g in loadedSections) {
			if (lowest == null) {
				lowest = g;
				continue;
			}

			if (lowest.transform.position.y > g.transform.position.y) {
				lowest = g;
			}

		}
		loadedSections.Remove (lowest);
		Destroy (lowest);
		numLoadedSections--;
	}

	// Update is called once per frame
	void Update () {
		float cam_pos_world = cameraPosition.position.y + numLoadedSectionsTarget + (incrementAmount.y / 2);
		if (cam_pos_world > currentGenWorldPosition.y) {
			if (numLoadedSections >= numLoadedSectionsTarget) {
				DestroyLowestSection ();
			}
			CreateSection (currentGenWorldPosition);
			currentGenWorldPosition += incrementAmount;
		}
	}
}
