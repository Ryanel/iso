﻿using UnityEngine;
using System.Collections;

public struct ReplayData
{
    public float frameTime;
    public Vector3 currentPosition;

    public bool playerInput;
    public bool cameraInput;

    public bool inputPlayerFwd;
    public bool inputPlayerBack;
    public bool inputPlayerLeft;
    public bool inputPlayerRight;

    public bool inputCameraUp;
    public bool inputCameraDown;
    public bool inputCameraLeft;
    public bool inputCameraRight;
}
