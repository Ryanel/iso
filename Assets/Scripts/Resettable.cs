﻿using UnityEngine;
using System.Collections;

public class Resettable : MonoBehaviour {
	
	Vector3 start_position;
	public bool deleteOnReset = false;

	LevelManager ls;
	//Things to reset
	TileMovement movement;
	Dropper dropper;
	Button button;
	Triggerable triggerable;
	Trigger trigger;
	// Use this for initialization
	void Start () {
		start_position = transform.position;
		movement = GetComponent<TileMovement> ();
		dropper = GetComponent<Dropper> ();
		button = GetComponent<Button> ();
		triggerable = GetComponent<Triggerable> ();
		trigger = GetComponent<Trigger> ();
		ls = GameObject.FindGameObjectWithTag("Level").GetComponent<LevelManager>();
	}
	
	void Update () {
        // If we reset
		if (ls.isResetting)
		{
            // Delete if we're spawned in
			if (deleteOnReset) {
				Destroy (gameObject);  
			}

            // Nevermind, let's reset every component.
			transform.position = start_position;
			if (movement != null) {
				movement.moveDeltaX = 0;
                movement.moveDeltaY = 0;
                movement.moveDeltaZ = 0;
                movement.currentPosition = start_position;
                //movement.targetPosition = start_position;
                //movement.worldPosition = start_position;
            }
			if (dropper != null) {
				dropper.hasBeenTriggered = false;
			}
			if (button != null) {
				button.isDepressed = false;
			}
			if (trigger != null) {
				trigger.SetState (false);
			}
			if (triggerable != null) {
				triggerable.isTriggered = false;
			}
		}
	}
}
