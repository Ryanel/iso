﻿using UnityEngine;
using System.Collections;

public class DirectionArrow : MonoBehaviour {
	public Material towards;
	public Material away;
	public CameraController direction_camera;
	Renderer r;
	int oldCamAngle;
	void Start()
	{
		r = GetComponent<Renderer> ();
		oldCamAngle = direction_camera.camAngle;
	}
	// Use this for initialization
	// Update is called once per frame
	void Update () {
		if(oldCamAngle == direction_camera.camAngle)
		{
			return;
		}

		oldCamAngle = direction_camera.camAngle;
		if (direction_camera.camAngle > 1) {
			r.material = away;
		} else {
			r.material = towards;
		}
	}
}
