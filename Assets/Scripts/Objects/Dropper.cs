﻿using UnityEngine;
using System.Collections;

public class Dropper : MonoBehaviour {
	public bool hasBeenTriggered = false;
	public GameObject objectToSpawn = null;
	public Vector3 spawn_position = new Vector3 (0, -1, 0);
	Triggerable trig;
	// Use this for initialization
	void Start () {
		trig = GetComponent<Triggerable> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (trig.isTriggered) {
			if (hasBeenTriggered == true) {
				return;
			}
			hasBeenTriggered = true;

			GameObject g = (GameObject)Instantiate(objectToSpawn, transform.position + spawn_position, Quaternion.Euler(Vector3.zero));
			g.name = objectToSpawn.name;
			Resettable reset = g.GetComponent<Resettable> ();
			if (reset != null) {
				reset.deleteOnReset = true;
			}

		}
	}
}
