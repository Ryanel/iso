﻿using UnityEngine;
using System.Collections;

public class ResetZone : MonoBehaviour {

    public bool failOnPlayer = true;
    public bool failOnBox = false;
    void OnTriggerEnter(Collider c)
	{
		if (c.CompareTag ("Player") && failOnPlayer) {
			GameObject.FindGameObjectWithTag ("Level").GetComponent<LevelManager> ().playerFailed = true;
		}
        if (c.CompareTag("Movable") && failOnBox)
        {
            GameObject.FindGameObjectWithTag("Level").GetComponent<LevelManager>().playerFailed = true;
        }
    }
}
