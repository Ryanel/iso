﻿using UnityEngine;
using System.Collections;

public class ToggleBarrier : MonoBehaviour {
	
    [Header("Options")]
	public bool toggleMode = false;
	public bool reverseState = false;
    [HideInInspector]
	public bool isActivated = false;

	bool lastState = false;

	public Material onMaterial;
	public Material offMaterial;

    private Triggerable trig;
    private BoxCollider col;
    private Renderer r;

    // Use this for initialization
    void Start () {
		trig = GetComponent<Triggerable> ();
		col = GetComponent<BoxCollider> ();
		r = GetComponent<Renderer> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (toggleMode == true) {
            //Toggle logic.
			if (trig.isTriggered != lastState) {
				if (trig.isTriggered == false) {
					lastState = !lastState;
				} else {
					lastState = !lastState;
					isActivated = !isActivated;
				}
			}
		} else {
            // Normal Logic.
			if (reverseState == false) {
				isActivated = trig.isTriggered;
			} else {
				isActivated = !trig.isTriggered;
			}
		}
        
        // Object Logic.
		col.enabled = isActivated;
        // FIXME: Don't do this every frame.
		if (isActivated) {
			r.material = onMaterial;
		} else {
			r.material = offMaterial;
		}
	}
}
