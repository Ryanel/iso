﻿using UnityEngine;
using System.Collections;

public class Goal : MonoBehaviour {
	void OnTriggerEnter(Collider c)
	{
		if (c.CompareTag ("Player")) {
			GameObject.FindGameObjectWithTag ("Level").GetComponent<LevelManager> ().hasReachedGoal = true;
		}
	}
}
