﻿using UnityEngine;
using System.Collections;

public class BoxScript : MonoBehaviour {

	TileMovement movement;
	
	void Start()
	{
		movement = GetComponent<TileMovement> ();
	}

	void OnCollisionEnter (Collision col)
	{
		//Abort early if we can
		if(col.gameObject.tag != "Player" && col.gameObject.tag != "Movable")
		{
			return;
		}
		if(movement.ignoreInputTicks > 0)
		{
			if (GameConfiguration.debugLog) { Debug.Log("[Collision]: Force ignored from " + col.gameObject.name + " -> " + this.name); }
			return;
		}
        // Determine direction
		Vector3 direction = col.contacts [0].normal;

        // Return if the collison is above or below the current plane.
		if (direction.y != 0 || col.gameObject.transform.position.y > transform.position.y + 0.75f) {
			return;
		}

		//We're moving. Where?
		if (direction.x > 0) {
			movement.moveDeltaX = 1;
		}
		if (direction.z > 0) {
			movement.moveDeltaZ = 1;
		}

		if (direction.x < 0) {
			movement.moveDeltaX = -1;
		}
		if (direction.z < 0) {
			movement.moveDeltaZ = -1;
		}
		if (GameConfiguration.debugLog) { Debug.Log("[Collision]: Force applied" + direction + " from " + col.gameObject.name + " -> " + this.name); }
	   
		//Apply relative cooldowns, and force a state update.
		if(col.gameObject.tag == "Player")
		{
			col.gameObject.GetComponent<TileMovement>().ignoreInputTicks += 25;
		}

		movement.ignoreInputTicks += 25;
	}

}
