﻿using UnityEngine;
using System.Collections;

public class JumpPad : MonoBehaviour {
    [Header("Options")]
	public Vector3 newPosition;
	int cooldown_frames = 0;
	// Use this for initialization

	void OnDrawGizmosSelected () {
		// Display the location of where we go when selected
		Gizmos.color = Color.green;
		Gizmos.DrawWireCube(transform.position + newPosition - new Vector3(0,0.5f,0), new Vector3(1,1,1));
	}

	void OnCollisionEnter (Collision col)
	{
		if (col.collider.CompareTag ("Player") && cooldown_frames == 0) {
			GameObject player = col.collider.gameObject;
			TileMovement d = player.GetComponent<TileMovement> ();
			Bounce(d);
		}
	}

	void OnCollisionStay(Collision col)
	{
		if (col.collider.CompareTag("Player") && cooldown_frames == 0)
		{
			GameObject player = col.collider.gameObject;
			TileMovement d = player.GetComponent<TileMovement>();
			Bounce(d);
		}
	}

	void Bounce(TileMovement t)
	{
		if (t.ignoreInputTicks > 0)
		{
			return;
		}
		t.moveDeltaX = (int)newPosition.x;
		t.moveDeltaZ = (int)newPosition.z;
		t.moveDeltaY = (int)newPosition.y;
		t.isStationary = false;
		cooldown_frames = 30;
	}

	void FixedUpdate()
	{
		if (cooldown_frames > 0) {
			cooldown_frames--;
		}
	}
}
