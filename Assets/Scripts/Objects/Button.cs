﻿using UnityEngine;
using System.Collections;

public class Button : MonoBehaviour {

    [Header("Options")]
    public bool playerCanActivate = true;
	public bool boxesCanActivate = true;
	public bool permanentlyActive = false;

    [Header("State")]
    public bool isDepressed = false;

    private Trigger trig;
    private AudioSource snd;

    // Use this for initialization
    void Start () {
		trig = GetComponent<Trigger> ();
        snd = GetComponent<AudioSource>();
	}

	void OnCollisionEnter (Collision col)
	{
		if (playerCanActivate) {
			if (col.collider.CompareTag ("Player")) {
				trig.SetState (true);
				isDepressed = true;

                snd.pitch = 1f;
                snd.Play();
            }
		}
		if (boxesCanActivate) {
			if (col.collider.name == "Box") {
				trig.SetState (true);
				isDepressed = true;

                snd.pitch = 1f;
                snd.Play();
            }
		}
	}
	void OnCollisionExit (Collision col)
	{
		if (permanentlyActive) {
			return;
		}
		if (playerCanActivate) {
			if (col.collider.CompareTag ("Player")) {
				trig.SetState (false);
				isDepressed = false;

                snd.pitch = 0.75f;
                snd.Play();
            }
		}
		if (boxesCanActivate) {
			if (col.collider.name == "Box") {
				trig.SetState (false);
				isDepressed = false;

                snd.pitch = 0.75f;
                snd.Play();
            }
		}
	}
}
