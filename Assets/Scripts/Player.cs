﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System.Collections.Generic;
public class Player : MonoBehaviour {

    [HideInInspector]
    public int camOrientation = 1;
    int control_scheme = 2; // 0 = no orientation change, 1 = controls reverse on down, 2 = front is up

    public TileMovement movement;
	private LevelManager level;
	private CameraController cam;

    const int ignoreInputTicks = 30;

    // Use this for initialization
    void Start () {
		movement = GetComponent<TileMovement> ();
		level = GameObject.FindGameObjectWithTag ("Level").GetComponent<LevelManager> ();
		cam = GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<CameraController> ();
		GameConfiguration.ReadConfig ();
		control_scheme = GameConfiguration.movementMode;
    }

    // Every time we move.
	void DoMovement()
	{
        // Are we on the ground, stationary, can move, and haven't failed the level?
		if (!movement.isStationary || !movement.isGrounded || movement.ignoreInputTicks > 0 || level.hasReachedGoal || level.playerFailed || level.playerDead) {
			return;
		}
        // Don't do anything if there isn't input
		if(!GameInput.playerForward && !GameInput.playerBackward && !GameInput.playerLeft && !GameInput.playerRight) {
			return;
		}

        bool moving_front = false;
        bool moving_back = false;   
        bool moving_left = false;
        bool moving_right = false;

        // Determine collisions
        RaycastHit hit_front;
		RaycastHit hit_back;
		RaycastHit hit_left;
		RaycastHit hit_right;
		bool blocked_front = Physics.Raycast (transform.position, new Vector3 (1, 0, 0), out hit_front, 0.5f);
		bool blocked_back  = Physics.Raycast (transform.position, new Vector3(-1,0,0), out hit_back, 0.5f);
		bool blocked_left  = Physics.Raycast (transform.position, new Vector3(0,0,1), out hit_left, 0.5f);
		bool blocked_right = Physics.Raycast (transform.position, new Vector3(0,0,-1), out hit_right, 0.5f);

        // Handle movable collions
		if (blocked_front) {
			if (hit_front.collider.CompareTag ("Movable")) {
				blocked_front = false;
			}
		}
		if (blocked_back) {
			if (hit_back.collider.CompareTag ("Movable")) {
				blocked_back = false;
			}
		}
		if (blocked_left) {
			if (hit_left.collider.CompareTag ("Movable")) {
				blocked_left = false;
			}
		}
		if (blocked_right) {
			if (hit_right.collider.CompareTag ("Movable")) {
				blocked_right = false;
			}
		}

        // Convert input to game movements

		// No orientation change
		if (control_scheme == 0) {
			if (GameInput.playerForward && !blocked_front) {
                moving_front = true;
			}
			if (GameInput.playerBackward && !blocked_back) {
                moving_back = true;
			}
			if (GameInput.playerLeft && !blocked_left) {
                moving_left = true;
			}
			if (GameInput.playerRight && !blocked_right) {
                moving_right = true;
			}
		}
		// Controls reverse on down
		if (control_scheme == 1) {

			if (cam.camAngle > 1)
			{
				camOrientation = 1;
			}
			else
			{
				camOrientation = 0;
			}

			if (GameInput.playerForward) {
				if (camOrientation == 0) {
					if (!blocked_front) {
                        moving_front = true;
					}
				} else {
					if (!blocked_back) {
                        moving_back = true;
					}
				}
			}
			if (GameInput.playerBackward) {
				if (camOrientation == 0) {
					if (!blocked_back) {
                        moving_back = true;
					}
				} else {
					if (!blocked_front) {
                        moving_front = true;
					}
				}
			}
			if (GameInput.playerLeft) {
				if (camOrientation == 0) {
					if (!blocked_left) {
                        moving_left = true;
					}
				} else {
					if (!blocked_right) {
                        moving_right = true;
					}
				}
			}
			if (GameInput.playerRight) {
				if (camOrientation == 0) {
					if (!blocked_right) {
                        moving_right = true;
					}
				} else {
					if (!blocked_left) {
                        moving_left = true;
					}
				}
			}
		}

		// Controls change every angle
		if (control_scheme == 2) {
			if (GameInput.playerForward) {
				switch (cam.camAngle) {
				case 0:
					if (!blocked_front) { moving_front = true; }
					break;
				case 1:
					if (!blocked_right) { moving_right = true; }
					break;
				case 2:
					if (!blocked_back)  { moving_back = true; }
					break;
				case 3:
					if (!blocked_left)  { moving_left = true; }
					break;
				}
			}
			if (GameInput.playerBackward) {
				switch (cam.camAngle) {
				case 0:
					if (!blocked_back)  { moving_back = true; }
					break;
				case 1:
					if (!blocked_left)  { moving_left = true; }
					break;
				case 2:
					if (!blocked_front) { moving_front = true; }
					break;
				case 3:
					if (!blocked_right) { moving_right = true; }
					break;
				}
			}
			if (GameInput.playerLeft) {
				switch (cam.camAngle) {
				case 0:
					if (!blocked_left)  { moving_left = true; }
					break;
				case 1:
					if (!blocked_front) { moving_front = true; }
					break;
				case 2:
					if (!blocked_right) { moving_right = true; }
					break;
				case 3:
					if (!blocked_back)  { moving_back = true; }
					break;
				}
			}
			if (GameInput.playerRight) {
				switch (cam.camAngle) {
				case 0:
					if (!blocked_right) { moving_right = true; }
					break;
				case 1:
					if (!blocked_back)  { moving_back = true; }
					break;
				case 2:
					if (!blocked_left)  { moving_left = true; }
					break;
				case 3:
					if (!blocked_front) { moving_front = true; }
					break;
				}
			}
		}

        // Handle game movements

        if(moving_front)
        {
            movement.moveDeltaX = 1;
            movement.ignoreInputTicks = ignoreInputTicks;
            CountMove();
        }
        if (moving_back)
        {
            movement.moveDeltaX = -1;
            movement.ignoreInputTicks = ignoreInputTicks;
            CountMove();
        }
        if (moving_left)
        {
            movement.moveDeltaZ = 1;
            movement.ignoreInputTicks = ignoreInputTicks;
            CountMove();
        }
        if (moving_right)
        {
            movement.moveDeltaZ = -1;
            movement.ignoreInputTicks = ignoreInputTicks;
            CountMove();
        }
    }

    void CountMove()
    {
        if(level.gameMode == LevelManager.GameMode.MOVE_LIMIT || level.gameMode == LevelManager.GameMode.MOVE_TIME_LIMIT)
        {
            level.movesLeft--;
        }
    }


	// Update is called once per frame
	void Update () {
		if (movement.isGrounded) {
			DoMovement ();
		}
	}
}
