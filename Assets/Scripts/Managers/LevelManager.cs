﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Analytics;

public class LevelManager : MonoBehaviour {
    [Header("Level Configuration")]
    public string levelName;
    public bool displayNameOnStart = true;
    public enum GameMode
    {
        COMPLETE_GOAL,
        TIME_LIMIT,
        TIME_SCORE,
        MOVE_LIMIT,
        MOVE_TIME_LIMIT,
        INFINITE
    }

    public GameMode gameMode;
    public float zoomLevel = 5.0f;

    // Timer
    [HideInInspector]
    public bool timerEnabled = true;
    public float initalTimeLimit;
    [HideInInspector]
    public float timer;

    // Move Limit
    public float initialMoveLimit;
    [HideInInspector]
    public float movesLeft;

    [Header("Game Status")]
    public bool hasReachedGoal = false;
    public bool playerDead = false;
    public bool playerFailed = false;
    [HideInInspector]
    public bool isResetting = false;
    
    private int analytics_resets = 0;
    private int resetTimerFrames = 2;

    private CameraController cam;
    private CameraController dircam;

    // Use this for initialization	
    void Start () {
        GameObject cam_go 		= GameObject.FindGameObjectWithTag ("MainCamera");
        GameObject dircam_go 	= GameObject.FindGameObjectWithTag ("DirectionCam");

        if (cam_go != null) {
            cam = cam_go.GetComponent<CameraController> ();
        }
        if (dircam_go != null) {
            dircam = dircam_go.GetComponent<CameraController> ();
        }

        // Set game settings every time we load a level.
		GameConfiguration.InitEngine();


        if(gameMode == GameMode.TIME_LIMIT || gameMode == GameMode.MOVE_TIME_LIMIT)
        {
            timer = initalTimeLimit;
        }
        
        if(gameMode == GameMode.MOVE_LIMIT || gameMode == GameMode.MOVE_TIME_LIMIT)
        {
            movesLeft = initialMoveLimit;
        }
    }

    // Reset Logic.
    public void Reset()
    {
        if (cam != null) {
            cam.distance = -30;
            cam.targetX = 45;
            cam.targetY = 45;
            cam.camAngle = 0;
        }

        if (dircam != null) {
            dircam.targetX = 45;
            dircam.targetY = 45;
            dircam.camAngle = 0;
        }

        GameObject.Find("UI").GetComponent<UIManager>().InitUI();

        playerFailed = false;
        playerDead = false;
        hasReachedGoal = false;
        timer = initalTimeLimit;
        movesLeft = initialMoveLimit;
        timerEnabled = true;

    }
    
    // Go back to the main menu
    public void GoBack()
    {
        //Report Statistics.
#if UNITY_ANALYTICS
        Analytics.SetUserId(SystemInfo.deviceUniqueIdentifier);
        Analytics.CustomEvent("levelEnd", new Dictionary<string, object>
        {
            { "level", levelName },
            { "totalResets", analytics_resets },
            { "levelFinished", hasReachedGoal },
            { "sceneName", UnityEngine.SceneManagement.SceneManager.GetActiveScene().name }
        });
       
#endif
        UnityEngine.SceneManagement.SceneManager.LoadScene ("MainMenu");
    }

    void FixedUpdate()
    {
        ResetLogic();
    }

    // Reset multiple times so that it sticks to all objects, and physics is stable.
    void ResetLogic()
    {
        if (isResetting)
        {
            if (resetTimerFrames > 0)
            {
                resetTimerFrames--;
                Reset();
            }
            else
            {
                isResetting = false;
            }
        }
    }

    // Process game rules and logic.
    void ProcessGameLogic()
    {
        // Unless we're resetting.
        if(isResetting == true)
        {
            return;
        }
        if (gameMode == GameMode.TIME_LIMIT || gameMode == GameMode.TIME_SCORE || gameMode == GameMode.MOVE_TIME_LIMIT)
        {
            timerEnabled = !hasReachedGoal;
            if(timerEnabled)
            {
                if (timer > 0)
                {
                    timer -= Time.deltaTime;
                    if(timer < 30)
                    {
                        if (timer - Mathf.Floor(timer) < 0.1f)
                        {
                            GetComponent<AudioSource>().Play();
                        }
                    }
                }
                else
                {
                    timer = 0;
                    playerFailed = true;
                }
            }
        }
        if (gameMode == GameMode.MOVE_LIMIT || gameMode == GameMode.MOVE_TIME_LIMIT)
        {
            if(movesLeft <= 0)
            {
                playerFailed = true;
            }
        }
    }

    //Check to see if we need to do anything.
    void ProcessInput()
    {
        if (GameInput.resetLevel)
        {
            isResetting = true;
            resetTimerFrames = 2;
            analytics_resets++;

        }
        if (GameInput.returnToMainMenu)
        {
            GoBack();
        }
    }

    // Update is called once per frame
    void Update () {
        ProcessInput();
        ProcessGameLogic();
    }
}
