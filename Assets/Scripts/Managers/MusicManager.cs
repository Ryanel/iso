﻿using UnityEngine;
using System.Collections;

public class MusicManager : MonoBehaviour {
	AudioSource audioS;
	// Use this for initialization
	void Start () {
		audioS = GetComponent<AudioSource> ();
		audioS.volume = GameConfiguration.musicVolume;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
