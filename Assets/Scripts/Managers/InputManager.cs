﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class InputManager : MonoBehaviour {
    
    
    const float mobileInputDeadzone = 1.75f;
    const float pcInputSensitivity = 0.25f;
    private CameraController mainCamera;
    private Player player = null;

    // Replay stuff.

    public bool replayRecording = true;
    public bool replayReplaying = false;
    public int replayDataTotalFrames = 0;
    public int replayCurrentFrame = 0;
    List<ReplayData> replayData;

    bool replayReplaySetup = false;
    float replayRecordTimestamp;
    public float replayTimestamp = 0;

    void Start() {
        mainCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraController>();
        GameObject p_g = GameObject.FindGameObjectWithTag("Player");

        if (p_g != null)
        {
            player = p_g.GetComponent<Player>();
        }

        if (player == null)
        {
            replayRecording = false;
            replayReplaying = false;
        }
        else
        {
            ReplayInit();
        }

    }

    void Update() {

        if (replayReplaying && !replayReplaySetup)
        {
            replayReplaySetup = true;
            replayTimestamp = Time.time;
            player.transform.position = replayData[0].currentPosition;
        }
        else
        {
            if(!replayReplaying)
            {
                replayReplaySetup = false;
            }
        }

        ProcessPlayerInput();
    }

    void ProcessPlayerInput() {
        GameInput.playerForward = false;
        GameInput.playerBackward = false;
        GameInput.playerLeft = false;
        GameInput.playerRight = false;

        if (replayReplaying)
        {
            ReplayReplay();
            return;
        }

        #if UNITY_ANDROID
		    ProcessInputMobile ();
        #else
            ProcessInputPC();
        #endif

        if(replayRecording)
        {
            ReplayRecord();
        }

    }
    void ProcessInputPC() {
        // Player
        GameInput.playerForward = Input.GetAxis("PlayerVertical") > pcInputSensitivity;
        GameInput.playerBackward = Input.GetAxis("PlayerVertical") < -pcInputSensitivity;
        GameInput.playerLeft = Input.GetAxis("PlayerHorizontal") < -pcInputSensitivity;
        GameInput.playerRight = Input.GetAxis("PlayerHorizontal") > pcInputSensitivity;

        // Camera
        GameInput.playerCameraLeft = Input.GetKeyDown(KeyCode.LeftArrow);
        GameInput.playerCameraRight = Input.GetKeyDown(KeyCode.RightArrow);
        GameInput.playerCameraUp = Input.GetKeyDown(KeyCode.UpArrow);
        GameInput.playerCameraDown = Input.GetKeyDown(KeyCode.DownArrow);

        // UI
        GameInput.resetLevel = Input.GetKeyDown(KeyCode.R);
        GameInput.returnToMainMenu = Input.GetKeyDown(KeyCode.Escape);

    }
    void ProcessInputMobile()
    {
        if (Input.touchCount > 0)
        {
            Touch locationTouch = Input.touches[0];
            if (IsPointerOverUIObject())
            {
                return;
            }
            Vector3 touchPos = new Vector3(locationTouch.position.x, locationTouch.position.y, mainCamera.distance);
            Vector3 desiredPosition = mainCamera.cam.ScreenToWorldPoint(touchPos);
            desiredPosition -= transform.position;

            if (desiredPosition.z > mobileInputDeadzone)
            {
                GameInput.playerLeft = true;
            }
            else if (desiredPosition.z < -mobileInputDeadzone)
            {
                GameInput.playerRight = true;
            }
            else if (desiredPosition.x > mobileInputDeadzone)
            {
                GameInput.playerForward = true;
            }
            else if (desiredPosition.x < -mobileInputDeadzone)
            {
                GameInput.playerBackward = true;
            }
        }
    }
    private bool IsPointerOverUIObject()
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }

    // Recording.
    void ReplayInit()
    {
        replayData = new List<ReplayData>(100);
        replayRecordTimestamp = Time.time;
        replayCurrentFrame = 0;
        replayDataTotalFrames = 0;
    }

    void ReplayRecord()
    {
        if (player == null)
        {
            return;
        }

        // Do not record this frame if we aren't inputting anything. This alone cuts down on most frames recorded and saves memory.
        if (!GameInput.playerForward && !GameInput.playerBackward && !GameInput.playerLeft && !GameInput.playerRight &&
            !GameInput.playerCameraUp && !GameInput.playerCameraDown && !GameInput.playerCameraLeft && !GameInput.playerCameraRight)
        {
            return;
        }

        // Do not record if the player can't accept input (unless it's a camera momement). This removes almost all of the "holding" or frames that don't affect input frames.
        if (!(GameInput.playerCameraUp || GameInput.playerCameraDown || GameInput.playerCameraLeft || GameInput.playerCameraRight) && (player.movement.isStationary && player.movement.isGrounded && !(player.movement.ignoreInputTicks > 0)))
        {   
            return;
        }

        // Looks like we need to record a frame.
        ReplayData replayFrame = new ReplayData();
        replayFrame.currentPosition = player.transform.position; // Save position so we can reset on desync
        replayFrame.frameTime = Time.time - replayRecordTimestamp; // Save time since recording started.
        // Is any of our inputs for the camera?
        replayFrame.cameraInput = (GameInput.playerCameraUp || GameInput.playerCameraDown || GameInput.playerCameraLeft || GameInput.playerCameraRight);
        // What about the player?
        replayFrame.playerInput = (GameInput.playerForward || GameInput.playerBackward || GameInput.playerLeft || GameInput.playerRight);

        // Save inputs for the Player
        if (replayFrame.playerInput)
        {
            replayFrame.inputPlayerFwd = GameInput.playerForward;
            replayFrame.inputPlayerBack = GameInput.playerBackward;
            replayFrame.inputPlayerLeft = GameInput.playerLeft;
            replayFrame.inputPlayerRight = GameInput.playerRight;
        }

        // Save inputs for the Camera
        if (replayFrame.cameraInput)
        {
            replayFrame.inputCameraUp = GameInput.playerCameraUp;
            replayFrame.inputCameraDown = GameInput.playerCameraDown;
            replayFrame.inputCameraLeft = GameInput.playerCameraLeft;
            replayFrame.inputCameraRight = GameInput.playerCameraRight;
        }

        //Update metadata
        replayData.Add(replayFrame);
        replayDataTotalFrames++;
    }

    void ReplayReplay()
    {

        if (replayCurrentFrame == replayData.Count)
        {
            replayReplaying = false;
            replayReplaySetup = false;

            GameInput.playerForward = false;
            GameInput.playerBackward = false;
            GameInput.playerLeft = false;
            GameInput.playerRight = false;
            GameInput.playerCameraUp = false;
            GameInput.playerCameraDown = false;
            GameInput.playerCameraLeft = false;
            GameInput.playerCameraRight = false;

            return;
        }

        //Check to see if the current "Timer" has reached the positon of the next replay frame, which are guarnteed be in order.

        if (replayData[replayCurrentFrame].frameTime < Time.time - replayTimestamp)
        {
            ReplayData replayFrame = replayData[replayCurrentFrame];

            GameInput.playerForward = replayFrame.inputPlayerFwd;
            GameInput.playerBackward = replayFrame.inputPlayerBack;
            GameInput.playerLeft = replayFrame.inputPlayerLeft;
            GameInput.playerRight = replayFrame.inputPlayerRight;

            GameInput.playerCameraUp = replayFrame.inputCameraUp;
            GameInput.playerCameraDown = replayFrame.inputCameraDown;
            GameInput.playerCameraLeft = replayFrame.inputCameraLeft;
            GameInput.playerCameraRight = replayFrame.inputCameraRight;

            replayCurrentFrame += 1;
        }
        else
        {
            GameInput.playerCameraUp = false;
            GameInput.playerCameraDown = false;
            GameInput.playerCameraLeft = false;
            GameInput.playerCameraRight = false;
        }
    }
}

public static class GameInput
{
    public static bool playerForward = false;
    public static bool playerBackward = false;
    public static bool playerLeft = false;
    public static bool playerRight = false;

    public static bool playerCameraUp = false;
    public static bool playerCameraDown = false;
    public static bool playerCameraLeft = false;
    public static bool playerCameraRight = false;

    public static bool returnToMainMenu = false;
    public static bool resetLevel = false;
}