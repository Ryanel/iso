﻿using UnityEngine;
using System.Collections;

public static class GameConfiguration {

	// Configuration
	public static int movementMode = 0;

	public static float musicVolume = 0.10f;

	public static bool appWasStarted = false;
    public static bool isReplaying = false;
	public static bool debugLog = false;

	// Tags
	private const string tag_movementMode = "SETTING_MOVEMENTMODE";
	private const string tag_musicVolume = "SETTING_MUSICVOLUME";

	private const string tag_version = "SETTING_VERSION";
	private const string tag_version_ok = "SETTING_OK";

	public static void WriteConfig()
	{
		PlayerPrefs.SetFloat (tag_musicVolume, musicVolume);
		PlayerPrefs.SetInt (tag_movementMode, movementMode);
		PlayerPrefs.Save ();
	}

	public static void ReadConfig()
	{
		PlayerPrefs.DeleteAll ();
		if (PlayerPrefs.GetString (tag_version) != tag_version_ok) {
			DefaultConfig ();
		}

		movementMode = PlayerPrefs.GetInt (tag_movementMode);
	}

	public static void DefaultConfig()
	{
		movementMode = 2;
		PlayerPrefs.SetString (tag_version, tag_version_ok);
		WriteConfig ();
	}


	public static void InitEngine()
	{
		ReadConfig ();
		Application.targetFrameRate = Screen.currentResolution.refreshRate + 15;
	}

}
