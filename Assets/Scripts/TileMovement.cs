﻿using UnityEngine;
using System.Collections;
#if true
public class TileMovement : MonoBehaviour {

    static private float movementDelta = 6f; // How fast we move, in units per second
    static private float fixupLimit = 0.1f; // How many units until we "snap" to the grid
    // Public

    [Header("Position")]
    // Requested movement deltas in every axis.
    public float moveDeltaX;
    public float moveDeltaY;
    public float moveDeltaZ;

    // Distance to the target position (current postion + movement deltas)
    float targetDistanceX;
    float targetDistanceY;
    float targetDistanceZ;
    private Vector3 lastPosition = Vector3.zero;
    [Header("State")]
    public bool isStationary = true; // We not moving, at all?
    public bool isColliding = false; // Are we colliding with anything on this frame
    public bool isGrounded = false; // Is their ground beneath us?

    [Header("Timing")]
    public int ignoreInputTicks = 0; //Ignore input for this many ticks. Counts down every frame.

    private Rigidbody rigidb;
    public Vector3 currentPosition;

    public void Start()
    {
        rigidb = GetComponent<Rigidbody>();
        CheckIfGrounded();
    }

    // Get the distance between 2 floats
    float GetDistance(float a, float b)
    {
        return Mathf.Abs(Mathf.Max(a, b) - Mathf.Min(a, b));
    }

    // Every GRAPHICAL frame.
    public void Update()
    {
        currentPosition = transform.position;
        DoMove();
    }

    // Main movement code
    public void DoMove()
    {
        float frameDeltaX = 0;
        float frameDeltaY = 0;
        float frameDeltaZ = 0;

        // Calculate the movement deltas to move THIS frame.
        if (moveDeltaX != 0)
        {
            frameDeltaX = Mathf.Lerp(currentPosition.x, currentPosition.x + moveDeltaX, movementDelta * Time.deltaTime) - currentPosition.x;
        }
        if (moveDeltaY != 0)
        {
            frameDeltaY = Mathf.Lerp(currentPosition.y, currentPosition.y + moveDeltaY, movementDelta * Time.deltaTime) - currentPosition.y;
        }
        if (moveDeltaZ != 0)
        {
            frameDeltaZ = Mathf.Lerp(currentPosition.z, currentPosition.z + moveDeltaZ, movementDelta * Time.deltaTime) - currentPosition.z;
        }
        
        // If we're not moving, do NOT set the y position to anything other then the current y. Allows gravity to work.
        if (frameDeltaY != 0)
        {
            currentPosition.y += frameDeltaY;
        }
        else
        {
            currentPosition.y = transform.position.y;
        }
        // Set positions.
        currentPosition.x += frameDeltaX;
        currentPosition.z += frameDeltaZ;

        // Update progress to movement delta.
        moveDeltaX -= frameDeltaX;
        moveDeltaY -= frameDeltaY;
        moveDeltaZ -= frameDeltaZ;

        // Make sure we snap to the grid if needed.
        FixupPosition();
        
        // Set positions on the game object
        lastPosition = transform.position;
        transform.position = currentPosition;
    }

    // Does a raycast to check if we're on the ground.
    public void CheckIfGrounded()
    {
        int layermask = 1 << base.gameObject.layer;
        layermask = ~layermask;
        RaycastHit raycastHit;
        isGrounded = Physics.Raycast(transform.position + new Vector3(0f, -(GetComponent<Renderer>().bounds.size.y / 4f), 0f), Vector3.down, out raycastHit, GetComponent<Renderer>().bounds.size.y / 2f, layermask);
    }

    // Updates status variables.
    public void UpdateStatus()
    {
        CheckIfGrounded();
        //isStationary = (targetDistanceX < fixupLimit && targetDistanceZ < fixupLimit && isGrounded) ? true : false;
        isStationary = lastPosition == transform.position;
    }

    // Every PHYSICS frame (the games internal tickrate)
    public void FixedUpdate()
    {
        if(ignoreInputTicks > 0)
        {
            ignoreInputTicks--;
        }
        UpdateStatus();
        // Don't let the rigidbody sleep.
        rigidb.WakeUp();
    }

    public void FixupPosition()
    {
        // Calculate target distances
        targetDistanceX = GetDistance(currentPosition.x, currentPosition.x + moveDeltaX);
        targetDistanceY = GetDistance(currentPosition.y, currentPosition.y + moveDeltaY);
        targetDistanceZ = GetDistance(currentPosition.z, currentPosition.z + moveDeltaZ);

        // If we're within the fixup limit, just set to the nearest whole number.
        if (targetDistanceX < fixupLimit)
        {
            currentPosition.x += moveDeltaX;
            moveDeltaX = 0;
            currentPosition.x = Mathf.Round(currentPosition.x);
        }

        if (targetDistanceY < fixupLimit)
        {
            currentPosition.y += moveDeltaY;
            moveDeltaY = 0;
        }

        if (targetDistanceZ < fixupLimit)
        {
            currentPosition.z += moveDeltaZ;
            moveDeltaZ = 0;
            currentPosition.z = Mathf.Round(currentPosition.z);
        }
    }
}
#endif