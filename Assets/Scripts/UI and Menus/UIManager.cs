﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class UIManager : MonoBehaviour {

    GameObject uiLosePanel;
    GameObject uiWinPanel;
    GameObject uiCounterPanel;
    GameObject uiMovesPanel;
    GameObject uiLevelNamePanel;
    Text uiLevelNameText;
    Text winPanelText;
    Text uiCounterText;
    Text uiMovesText;
    // Data references

    LevelManager levelScript;

    // Counters
    public int animOSDFade = 240;
    // Use this for initialization
    void Start () {
        levelScript = GameObject.FindGameObjectWithTag("Level").GetComponent<LevelManager>();

        // Get UI Objects
        uiLosePanel = GameObject.Find("UILosePanel");
        uiWinPanel = GameObject.Find("UIWinPanel");
        uiCounterPanel = GameObject.Find("UICounterPanel");
        uiLevelNamePanel = GameObject.Find("UILevelNamePanel");
        uiMovesPanel = GameObject.Find("UIMovesPanel");
        // Get Texts
        uiLevelNameText = GameObject.Find("UILevelNameText").GetComponent<Text>();
        winPanelText = GameObject.Find("UIWinStageName").GetComponent<Text>();
        uiCounterText = GameObject.Find("UICounterText").GetComponent<Text>();
        uiMovesText = GameObject.Find("UIMovesText").GetComponent<Text>();
        InitUI();
    }
    
    // Update is called once per frame
    void Update () {
        UpdateUI();
    }

    void FixedUpdate()
    {
        if (animOSDFade > 0)
        {
            animOSDFade--;
        }
    }

    public void InitUI()
    {
        // Level
        uiLevelNamePanel.SetActive(levelScript.displayNameOnStart);
        uiLevelNameText.text = levelScript.levelName;
        // Win
        winPanelText.text = levelScript.levelName + " - Completed.";
        // Counter
        uiCounterPanel.SetActive(levelScript.gameMode == LevelManager.GameMode.TIME_LIMIT || levelScript.gameMode == LevelManager.GameMode.MOVE_TIME_LIMIT);
        uiMovesPanel.SetActive(levelScript.gameMode == LevelManager.GameMode.MOVE_LIMIT || levelScript.gameMode == LevelManager.GameMode.MOVE_TIME_LIMIT);
        animOSDFade = 240;
    }

    public void UpdateUI()
    {
        uiWinPanel.SetActive(levelScript.hasReachedGoal && !(levelScript.playerFailed || levelScript.playerDead));
        uiLosePanel.SetActive(levelScript.playerFailed || levelScript.playerDead);
        //Timing
        if (uiCounterPanel.activeInHierarchy)
        {
            uiCounterText.text = levelScript.timer.ToString("F");
        }

        if (uiMovesPanel.activeInHierarchy)
        {
            uiMovesText.text = levelScript.movesLeft.ToString();
        }

        if (animOSDFade > 0)
        {
            uiLevelNamePanel.GetComponent<CanvasRenderer>().SetAlpha(animOSDFade / 255f);
        }
        else
        {
            if (animOSDFade == 0)
            {
                animOSDFade = -1;
                uiLevelNamePanel.SetActive(false);
            }
        }
    }

}
