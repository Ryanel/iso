﻿using UnityEngine;
using System.Collections;

public class MovementOptionButton : MonoBehaviour {
	public int option;
	void OnMouseDown()
	{
		GameConfiguration.movementMode = option;
		GameConfiguration.WriteConfig ();
	}
}
