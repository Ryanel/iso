﻿using UnityEngine;
using System.Collections;

public class SelectSubmenu : MonoBehaviour {

	public Transform target;

	CameraController cam;

	// Use this for initialization
	void Start () {
		GameObject cam_go = GameObject.FindGameObjectWithTag("MainCamera");
		cam = cam_go.GetComponent<CameraController>();

	}
	void OnMouseDown()
	{
		cam.target = target;
	}
}
