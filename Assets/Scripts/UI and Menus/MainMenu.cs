﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Analytics;

public class MainMenu : MonoBehaviour
{

    public UnityEngine.UI.Text tooltipText;
    public CameraController menuCamera;
    // Use this for initialization
    void Start()
    {
        if (GameConfiguration.appWasStarted == false)
        {
            GameConfiguration.appWasStarted = true;
#if UNITY_ANALYTICS
            Analytics.SetUserId(SystemInfo.deviceUniqueIdentifier);
            Analytics.CustomEvent("AppStarted", new Dictionary<string, object>
            {
                { "Version", Application.version },
                { "Genuine", Application.genuine },
                { "Lang", Application.systemLanguage },
                { "GPU", SystemInfo.graphicsDeviceName },
                { "OS", SystemInfo.operatingSystem },
                { "Type", SystemInfo.deviceType }
            });
#endif
        }
    }
}
