﻿using UnityEngine;
using System.Collections;

public class Trigger : MonoBehaviour {
	public Triggerable[] targets;
	void OnDrawGizmosSelected () {
		// Display the explosion radius when selected
		Gizmos.color = Color.red;
		foreach (Triggerable t in targets) {
			Gizmos.DrawLine (transform.position, t.transform.position);
		}
	}

	public void SetState(bool state)
	{
		foreach (Triggerable t in targets) {
			t.isTriggered = state;
		}
	}
}
