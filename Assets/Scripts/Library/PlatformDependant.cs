﻿using UnityEngine;
using System.Collections;

public class PlatformDependant : MonoBehaviour {
	public bool mobileOnly = true;
    public bool standaloneOnly = false;
    public bool webGLOnly = false;
    // Use this for initialization
    void Start () {
#if UNITY_STANDALONE
        if(standaloneOnly)
        {
            this.gameObject.SetActive(true);
        }
		
        if(mobileOnly)
        {
            this.gameObject.SetActive(false);
        }
#elif UNITY_WEBGL // WebGL acts like standalone
        if (standaloneOnly)
        {
            this.gameObject.SetActive(true);
        }

        if (mobileOnly)
        {
            this.gameObject.SetActive(false);
        }
#else
        if(standaloneOnly)
        {
            this.gameObject.SetActive(false);
        }
		
        if(mobileOnly)
        {
            this.gameObject.SetActive(true);
        }
#endif
    }
}
