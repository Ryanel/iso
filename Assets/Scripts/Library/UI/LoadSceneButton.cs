﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
public class LoadSceneButton : MonoBehaviour {
    public string levelToLoad = "";
    // Use this for initialization
    void OnMouseDown()
    {
        //foreach (GameObject obj in GameObject.FindGameObjectsWithTag("LoadUI"))
        //{
            //obj.SetActive(true);
        //}
        SceneManager.LoadScene (levelToLoad);
        //StartCoroutine("AsynchronousLoad");
    }
    
    IEnumerator AsynchronousLoad()
    {
        yield return null;

        AsyncOperation ao = SceneManager.LoadSceneAsync(levelToLoad,LoadSceneMode.Single);
        ao.allowSceneActivation = false;

        while (!ao.isDone)
        {
            // [0, 0.9] > [0, 1]
            float progress = Mathf.Clamp01(ao.progress / 0.9f);
            if (progress != 1)
            {
                Debug.Log("Loading progress: " + (progress * 100) + "%");
            }
            

            // Loading completed
            if (ao.progress == 0.9f)
            {
                Debug.Log("Press a key to start");
                
                if (Input.GetKeyDown(KeyCode.Space))
                    ao.allowSceneActivation = true;
            }

            yield return null;
        }
    }
    
}
