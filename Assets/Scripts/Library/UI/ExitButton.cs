﻿using UnityEngine;
using System.Collections;

public class ExitButton : MonoBehaviour {
	void OnMouseDown()
	{
		#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
		#else
		Application.Quit();
		#endif
	}
}
