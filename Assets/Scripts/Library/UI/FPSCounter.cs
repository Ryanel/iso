﻿/* **************************************************************************
 * FPS COUNTER
 * **************************************************************************
 * Written by: Annop "Nargus" Prapasapong
 * Created: 7 June 2012
 * *************************************************************************/

using UnityEngine;
using System.Collections;

/* **************************************************************************
 * CLASS: FPS COUNTER
 * *************************************************************************/
public class FPSCounter : MonoBehaviour
{
	/* Public Variables */
	public float frequency = 0.5f;

	/* **********************************************************************
	 * PROPERTIES
	 * *********************************************************************/
	public int FramesPerSec { get; protected set; }

	/* **********************************************************************
	 * EVENT HANDLERS
	 * *********************************************************************/
	/*
	 * EVENT: Start
	 */

	UnityEngine.UI.Text text;
	private void Start()
	{
		text = gameObject.GetComponent<UnityEngine.UI.Text>();
		StartCoroutine(FPS());
	}

	/*
	 * EVENT: FPS
	 */
	private IEnumerator FPS()
	{
		for (;;)
		{
			// Capture frame-per-second
			int lastFrameCount = Time.frameCount;
			float lastTime = Time.realtimeSinceStartup;
			yield return new WaitForSeconds(frequency);
			float timeSpan = Time.realtimeSinceStartup - lastTime;
			int frameCount = Time.frameCount - lastFrameCount;

			// Display it
			FramesPerSec = Mathf.RoundToInt(frameCount / timeSpan);
			text.text = FramesPerSec.ToString() + " fps";
			if (FramesPerSec >= 120)
			{
				text.color = Color.blue;
			}
			else if (FramesPerSec >= 60)
			{
				text.color = Color.white;
			}
			else if (FramesPerSec >= 30)
			{
				text.color = Color.yellow;
			}
			else
			{
				text.color = Color.red;
			}
		}
	}
}