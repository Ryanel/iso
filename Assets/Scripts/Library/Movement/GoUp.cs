﻿using UnityEngine;
using System.Collections;

public class GoUp : MonoBehaviour {

	public float speed = 0.1f;
	// Update is called once per frame
	void FixedUpdate () {
		transform.position = new Vector3 (transform.position.x, transform.position.y + speed, transform.position.z);
	}
}
